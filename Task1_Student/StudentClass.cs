﻿using System;
using System.Collections.Generic;

namespace Task1_Student
{
    class StudentClass 
    {
        private string name = "Kate";
        private string gender = "female";
        int counter = 1;
        private List<string> skills = new List<string> { };
        StudentAddress address;

        public StudentAddress StudentsAdress { get { return address; } }
        public string Name { get { return name; } }
        public string Gender { get { return gender; } }

        public StudentClass(string name, string gender)
        {
            Console.WriteLine("Custom constructor");
            this.name = name;
            this.gender = gender;
        }

        public StudentClass(StudentAddress studentAddress)
        {
            this.address = studentAddress;
        }

        public void AddSkill(string language)
        {
            Console.WriteLine("Now you are going to learn {0}!", language);
            skills.Add(language);
            counter++;
        }

        public bool CheckSkill(string language)
        {
            bool result = true;

            Console.WriteLine("Do you know {0}?", language);

            for (int i = 0; i < skills.Count; i++)
            {
                if (skills[i] == language)
                {
                    Console.WriteLine("Good.You know {0}", language);
                    result = true;
                }
            }
            return result;
        }
    }
}
