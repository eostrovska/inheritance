﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1_Student
{
    class StudentAddress 
    {
        private string country = "";
        private string city = "";
        private string street = "";
        private int houseNumber = 0;
        private int roomNumber = 0;

        public string Country { get { return country; } }
        public string City { get { return city; } }
        public string Street { get { return street; } }
        public int HouseNumber { get { return houseNumber; } }
        public int RoomNumber { get { return roomNumber; } }

        public StudentAddress(string country, string city, string street, int houseNumber, int roomNumber)
        {
            this.country = country;
            this.city = city;
            this.street = street;
            this.houseNumber = houseNumber;
            this.roomNumber = roomNumber;
        }
    }
}
