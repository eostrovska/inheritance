﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1_Student
{
    class Program
    {
        static void Main(string[] args)
        {
            StudentAddress studentAdress = new StudentAddress("Ukraine", "Kyiv", "Kievskaya", 345, 45);
            StudentClass student = new StudentClass(studentAdress);
            StudentClass student2 = new StudentClass("Ivan", "male");

            Console.WriteLine("Task 1: Continue the task considered in the lesson with the class Student.Add a field of type Address to the class Student." +
                              "Address is a class in which there are such fields: country, city, street, house, room." +
                              "You need for the class Student to add custom constructor that takes an argument of type Address and" +
                              "initializes the corresponding field for the student.");
            Console.WriteLine(new string('-', 60));

            student.AddSkill("c#");
            student.AddSkill("sql");
            student.AddSkill("html");
            student.CheckSkill("c#");
            Console.WriteLine("Student {0} is {1} ", student.Name, student.Gender);
            Console.WriteLine("And lives in {0}, city {1}, {2} street {3}/{4}", student.StudentsAdress.Country, student.StudentsAdress.City, 
                               student.StudentsAdress.Street, student.StudentsAdress.HouseNumber, student.StudentsAdress.RoomNumber);
            Console.WriteLine("Student {0} is {1} ", student2.Name, student2.Gender);
            Console.ReadKey();
        }
    }
}
