﻿using System;

namespace Task2_BinaryLogiс
{
    class Program
    {
        static void Main(string[] args)
        {
            byte operand1 = 230;
            byte operand2 = 45;
            byte numberOfShifting = 4;

            Console.WriteLine("Task 2: Create class to do logical bits operations(conjunction, disjunction, operator OR, logical shift)." +
                              "Create its instance and in the specified methods pass numbers to be carried out the corresponding actions." +
                              "And display to Console the results of the conjunction,disjunction, etc.");

            BinaryLogicOperations resultOfBinaryLogicOperation = new BinaryLogicOperations();
            resultOfBinaryLogicOperation.Conjunction(operand1, operand2);
            resultOfBinaryLogicOperation.Disjunction(operand1, operand2);
            resultOfBinaryLogicOperation.XOR(operand1, operand2);
            resultOfBinaryLogicOperation.LogicalLeftShift(operand1, numberOfShifting);
            resultOfBinaryLogicOperation.LogicalRightShift(operand2, numberOfShifting);

            Console.ReadKey();
        }
    }
}
