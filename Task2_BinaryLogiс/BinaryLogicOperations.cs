﻿using System;

namespace Task2_BinaryLogiс
{
    class BinaryLogicOperations
    {
        private int result;

        public void Conjunction(byte operand1, byte operand2)
        {
            Console.WriteLine("Conjunction:");
            result = operand1 & operand2;
            Console.WriteLine("{0} AND {1} is {2}", operand1, operand2, result);
        }

        public void Disjunction(byte operand1, byte operand2)
        {
            Console.WriteLine("Disjunction:");
            result = operand1 | operand2;
            Console.WriteLine("{0} OR {1} is {2}", operand1, operand2, result);
        }

        public void XOR(byte operand1, byte operand2)
        {
            Console.WriteLine("XOR:");
            result = operand1 ^ operand2;
            Console.WriteLine("{0} XOR {1} is {2}", operand1, operand2, result);
        }

        public void LogicalRightShift(byte operand, byte numberOfShifting)
        {
            Console.WriteLine("Logical Right Shift:");
            result = operand >> numberOfShifting;
            Console.WriteLine("{0} >> {1} is {2}", operand, numberOfShifting, result);
        }

        public void LogicalLeftShift(byte operand, byte numberOfShifting)
        {
            Console.WriteLine("Logical Left Shift:");
            result = operand << numberOfShifting;
            Console.WriteLine("{0} << {1} is {2}", operand, numberOfShifting, result);
        }
    }
}
