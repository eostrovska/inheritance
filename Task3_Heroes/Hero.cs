﻿using System;

namespace Task3_Heroes
{
    class Hero
    {
        private string name;
        private int age;
        private string gender;

        public string Name { get { return name; } }
        public int Age { get { return age; } }
        public string Gender { get { return gender; } }

        public Hero(string name, int age, string gender)
        {
            this.name = name;
            this.age = age;
            this.gender = gender;
        }

        internal void Levitation()
        {
            Console.WriteLine("This hero can levitate");
        }

        internal void DescriptionOfHero()
        {
            Console.WriteLine("This is {0}, {1} and his/her age is {2}", name, gender, age );
        }
    }
}
