﻿using System;

namespace Task3_Heroes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Task 3: About superHeroes.");
            Console.WriteLine(new string('-', 60));

            HeroWIthGun heroWithGun = new HeroWIthGun("Colt M1911");
            heroWithGun.DescriptionOfHero();
            heroWithGun.Levitation();
            heroWithGun.Shootting(2);
            heroWithGun.GunReloading(4);

            Console.WriteLine(new string('-', 60));

            Sage sage = new Sage();
            sage.DescriptionOfHero();
            sage.Levitation();
            sage.Reading("Three friend");
            sage.AskQuestion();

            Console.WriteLine(new string('-', 60));

            Witch kievWitch = new Witch();
            kievWitch.DescriptionOfHero();
            kievWitch.Levitation();
            kievWitch.BoilPotion();

            Console.ReadKey();
        }
    }
}
