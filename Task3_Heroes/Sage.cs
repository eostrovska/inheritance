﻿using System;

namespace Task3_Heroes
{
    class Sage : Hero
    {
        string [] wiseSaying = { "Learning, learning and learning more",
                                 "If you don't know what to do, you would go to sleep",
                                 "It is earsy! Just live!"};

        public Sage() : base("Miraton", 230, "male")
        {
            Console.WriteLine("He has a lot of knowledge");
        }

        public void Reading(string book)
        {
            Console.WriteLine("{0} is reading th book {1}", Name, book);
        }

        public void AskQuestion()
        {
            string question = Console.ReadLine();

            if(question != null)
            {
                Random rand = new Random();
                int i = rand.Next(0, 2);
                Console.WriteLine(wiseSaying[i]);
            }
        }
    }
}
