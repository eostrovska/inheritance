﻿using System;

namespace Task3_Heroes
{
    class HeroWIthGun : Hero
    {
        string gun;
        int ammunition;

        public string Gun { get { return gun; } }

        public HeroWIthGun(string gun) : base("Duglas", 23, "male")
        {
            this.gun = gun;
            Console.WriteLine("He has {0}", gun);
        }

        public void Shootting(int ammunition)
        {
            Console.WriteLine("He shootting!");

            this.ammunition = ammunition;

            for (int i = 0; i < ammunition; i++)
            {
                Console.WriteLine("Pif - paf");
            }
        }

        public void GunReloading(int ammunition)
        {
            Console.WriteLine("Gun Reloading");

            this.ammunition = ammunition;
            Shootting(ammunition);
        }
    }
}
