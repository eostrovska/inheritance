﻿using System;

namespace Task3_Heroes
{
    class Witch : Hero
    {
        public Witch() : base("BedRed", 1730, "female")
        {
            Console.WriteLine("She has a lot of witch power");
        }

        public void BoilPotion()
        {
            Console.WriteLine("Enter '1' - if you want to be invisible");
            Console.WriteLine("Enter '2' - if you want to eat a lot and willn't get fat");
            string wish = Console.ReadLine();

            switch (wish)
            {
                case "1":
                    for(int i = 1; i< 4; i++)
                    {
                        Console.WriteLine(i);
                    }
                    Console.WriteLine("Now, you are invisible! Congratulations!");
                    break;
                case "2":
                    Console.WriteLine("Do not be a summer student. Keep a diet and go in for sports. Everything is simple");
                    break;
                default:
                    Console.WriteLine("Determined what you want");
                    break;
            }
        }
    }
}
